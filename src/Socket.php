<?php

namespace PHPSockets;

class Socket {

    function __construct(protected \Socket $socket) {}

    public static function create (int $domain, int $type, int $protocol): \Socket | false {
        return socket_create($domain, $type, $protocol);
    }

    public function bind (string $host, int $port): bool {
        return socket_bind($this->socket, $host, $port);
    }

    public function listen (int $backlog = 0) {
        return socket_listen($this->socket, $backlog);
    }

    public function accept (): Socket | false {
        $socket = socket_accept($this->socket);

        if ($socket) {
            return new Socket($socket);
        }

        return $socket;
    }

    public function write(string $buffer, $length = null): int | false {
        return socket_write($this->socket, $buffer, $length);
    }

    public function read (int $length, int $type = PHP_BINARY_READ): string | false {
        return socket_read($this->socket, $length, $type);
    }

    public function close (): void {
        socket_close($this->socket);
    }

}
