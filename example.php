<?php

require_once __DIR__ . '/vendor/autoload.php';

use PHPSockets\Socket;

$host = '127.0.0.1';
$port = 12345;

$server = new Socket(Socket::create(AF_INET, SOCK_STREAM, SOL_TCP));
$server->bind($host, $port);
$server->listen();

while(true) {
    $clientSocket = $server->accept();
    $message = "Bem-vindo ao servidor!\n";
    $clientSocket->write($message, strlen($message));
    while (true) {
        $data = $clientSocket->read(1024);
        if (!$data) break;
        echo $data;
        $response = "Mensagem recebida: " . $data . "\n";
        $clientSocket->write($response, strlen($response));
    }
    $clientSocket->close();
}
