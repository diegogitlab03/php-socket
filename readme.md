## Class `PHPSockets\Socket`

The `Socket` class provides a wrapper around PHP's socket functions, allowing you to create and manage socket connections in an object-oriented way.

### Constructor

```php
__construct(Socket $socket)
```

The constructor creates a new Socket instance using an existing PHP socket resource.

Parameters
- `$socket` (required): A PHP socket resource created with socket_create or `socket_accept`.

### Static Method

```php
create(int $domain, int $type, int $protocol): \Socket|false
```

The `create` method creates a new socket resource.

Parameters

- `$domain`(required): The address family for the socket, either `AF_INET` for IPv4 or `AF_INET6` for IPv6.
- `$type` (required): The type of socket, either `SOCK_STREAM` for a TCP socket or `SOCK_DGRAM` for a UDP socket.
- `$protocol` (required): The transport protocol to use, either `SOL_TCP` for TCP or `SOL_UDP` for UDP.

Return Value

- Returns a PHP socket resource if successful, or `false` on failure.

### Method

```php
bind(string $host, int $port): bool
```

The `bind` method binds a socket resource to a specific IP address and port.

Parameters

- `$host`(required): The IP address to bind the socket to.
- `$port` (required): The port number to bind the socket to.

Return Value

- Returns true if successful, or false on failure.

### Method

```php
listen(int $backlog = 0)
```

The `listen` method puts a socket resource into listening mode, waiting for incoming connections.

Parameters

- `$backlog` (optional): The maximum number of pending connections to allow.

### Method

```php
accept(): Socket | false
```

The `accept` method accepts an incoming connection and returns a new Socket instance for the connection.

Return Value

- Returns a new `Socket` instance if successful, or `false` on failure.

### Method

```php
write(string $buffer, int $length = null): int | false
```

The `write` method writes data to a socket resource.

Parameters

- `$buffer`(required): The data to write to the socket.
- `$length` (optional): The maximum number of bytes to write. If omitted, all data in the buffer will be written.

Return Value

- Returns the number of bytes written if successful, or `false` on failure.

### Method

```php
read(int $length, int $type = PHP_BINARY_READ): string | false
```

The `read` method reads data from a socket resource.

Parameters
- `$length` (required): The maximum number of bytes to read.
- `$type` (optional): The type of data to read, either `PHP_BINARY_READ` for binary data or `PHP_NORMAL_READ` for line-oriented data.

Return Value

- Returns the data read from the socket, or `false` on failure.

### Method

```php
close(): void
```

The `close` method closes the socket connection.

Return Value
- Does not return a value.
